﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Bootstrapper
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void Initialize()
    {
        ServiceLocator.Initialize();

        ServiceLocator.serviceLocatorInstance.Get<analyticDevToDev>();

        SceneManager.LoadSceneAsync(0);
    }
}