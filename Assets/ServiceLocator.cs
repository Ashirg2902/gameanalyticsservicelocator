﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServiceLocator : MonoBehaviour
{
    private Dictionary<string, IGameService> services = new Dictionary<string, IGameService>();
    public static ServiceLocator serviceLocatorInstance { get; private set; }

    public static void Initialize()
    {
        serviceLocatorInstance = new ServiceLocator();
    }
    public T Get<T>() where T : IGameService
    {
        string key = typeof(T).Name;
        if (!services.ContainsKey(key))
        {
            Debug.LogError($"Key {key} in not registrated!");
            throw new InvalidOperationException();
        }
        return (T)services[key];
    }
    public void Register<T>(T servise) where T : IGameService
    {
        string key = typeof(T).Name;
        if (services.ContainsKey(key))
        {
            Debug.LogError($"Key {key} in not initialized!");
            return;
        }
        services.Add(key, servise);
    }
    public void UnityRegister<T>() where T : IGameService
    {
        string key = typeof(T).Name;
        if (!services.ContainsKey(key))
        {
            Debug.LogError($"Key {key} in not registrated!");
            return;
        }
        services.Remove(key);
    }
}