﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceType : MonoBehaviour
{
    public static ResourceType instance;
    public float Gold;
    public float House;
    public float People;
    private void Awake()
    {
        instance = this;
    }

}