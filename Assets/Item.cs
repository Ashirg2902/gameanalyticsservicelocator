﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField] private float weight;
    public float Weight => weight;
    [SerializeField] private int id;
    public int Id => id;
#if UNITY_EDITOR
    private void OnValidate()
    {
        if (weight < 0)
            weight = 0;
        if (id < 0)
            id = 0;
    }
#endif
}