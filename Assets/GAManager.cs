﻿using GameAnalyticsSDK;
using UnityEngine;

public class GAManager : MonoBehaviour
{
    public static GAManager instance;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        GameAnalytics.Initialize();
    }

    public void Check(int _level)
    {
        GameAnalytics.NewProgressionEvent(GAProgressionStatus.Complete, _level.ToString());
        Debug.Log("Level" + _level);
    }
}
